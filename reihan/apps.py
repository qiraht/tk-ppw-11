from django.apps import AppConfig


class ReihanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'reihan'
