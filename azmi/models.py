from django.db import models

# Create your models here.
class inputBarangModel(models.Model):
    title = models.CharField(max_length=50, null=True)
    items_pic = models.ImageField(upload_to='images/', null=True, blank=True)
    description = models.CharField(max_length=50, blank=True)