from .models import inputBarangModel
from django import forms

class inputBarangForm(forms.ModelForm):
    class Meta:
        model = inputBarangModel
        fields = ['title', 'items_pic', 'description']