from django.shortcuts import render, redirect

from .forms import inputBarangForm

# Create your views here.
def inputBarang_page(request):
    
    form = inputBarangForm()
    if request.method == 'POST':
        form = inputBarangForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    context= {'form':form}

    return render(request, 'home/inputBarang.html', context)
